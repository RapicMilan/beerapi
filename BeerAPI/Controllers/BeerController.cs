﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BeerAPI.Models;

namespace BeerAPI.Controllers
{
    public class BeerController : ApiController
    {
        static readonly IBeerRepository repository = new BeerRepository();

        // GET api/beer
        [HttpGet]
        public IEnumerable<Beer> Get()
        {
            return repository.GetAll();
        }

        // GET api/beer/5
        [HttpGet]
        [Route("api/beer/{id:int}")]
        public IHttpActionResult Get(int id)
        {
            Beer beer = repository.Get(id);
            if (beer == null)
            {
                return NotFound();
            }
            return Ok(beer);
        }

        // GET api/beer/jelen
        [HttpGet]
        [Route("api/beer/{search}")]
        public IEnumerable<Beer> Get(string search)
        {
            return repository.GetByName(search);
        }

        // POST api/beer
        [HttpPost]
        public HttpResponseMessage Post([FromBody]Beer beer)
        {
            repository.Add(beer);
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        // PUT api/beer/5
        [HttpPut]
        public IHttpActionResult Put(int id, [FromBody]Beer beer)
        {
            repository.Update(beer);
            return Ok();
        }

        // DELETE api/beer/5
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            repository.Remove(id);
            return Ok();
        }
    }
}
