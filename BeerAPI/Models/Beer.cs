﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeerAPI.Models
{
    public class Beer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public uint Rating { get; set; }
    }
}