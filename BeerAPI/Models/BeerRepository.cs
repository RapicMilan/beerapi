﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeerAPI.Models
{
    public class BeerRepository : IBeerRepository
    {
        private List<Beer> beers = new List<Beer>();
        private int _nextId = 1;

        public BeerRepository()
        {
            Add(new Beer { Name = "Jelen", Type = "Light", Rating = 5 });
            Add(new Beer { Name = "Lav", Type = "Light", Rating = 3 });
            Add(new Beer { Name = "Beck's", Type = "Light", Rating = 3 });
            Add(new Beer { Name = "Tuborg", Type = "Light", Rating = 4 });
            Add(new Beer { Name = "Heineken", Type = "Light", Rating = 5 });
            Add(new Beer { Name = "Budweiser", Type = "Light", Rating = 4 });
            Add(new Beer { Name = "Lasko", Type = "Dark", Rating = 3 });
            Add(new Beer { Name = "Lasko", Type = "Light", Rating = 3 });            
        }
    

        public Beer Add(Beer item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            item.ID = _nextId++;
            beers.Add(item);

            return item;
        }

        public Beer Get(int id)
        {
            return beers.Find(x => x.ID == id);
        }

        public IEnumerable<Beer> GetAll()
        {
            return beers;
        }

        public IEnumerable<Beer> GetByName(string search)
        {
            return beers.Where(x => x.Name.ToLower().Contains(search.ToLower()));
        }

        public void Remove(int id)
        {
            beers.RemoveAll(x => x.ID == id);
        }

        public bool Update(Beer item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            int index = beers.FindIndex(x => x.ID == item.ID);
            if (index == -1)
            {
                return false;
            }

            // Update rating, save average value
            Beer beer = beers.Find(x => x.ID == item.ID);
            if (beer.Rating != 0)
            {
                // Make sure that maximum rating is 5
                uint newRating = item.Rating > 5 ? 5 : item.Rating; 
                item.Rating = (uint)Math.Round((double)(beer.Rating + newRating) / 2);
            }

            beers.RemoveAt(index);
            beers.Add(item);

            return true;
        }
    }
}