﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerAPI.Models
{
    public interface IBeerRepository
    {
        IEnumerable<Beer> GetAll();
        Beer Get(int id);
        IEnumerable<Beer> GetByName(string search);
        Beer Add(Beer item);
        void Remove(int id);
        bool Update(Beer item);
    }
}
